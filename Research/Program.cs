using Research;
using Research.Repository.Base;
using Research.Repository.Interfaces;
using Research.Repository.Repositories;

var builder = WebApplication.CreateBuilder(args);
var startup = new Startup(builder.Configuration);

// Add services to the container.
builder.Services.AddRazorPages();

builder.Services.AddAutoMapper(typeof(Startup));

#region Repositories
builder.Services.AddSingleton(typeof(IGenericRepository<>), typeof(BaseRepository<>));
builder.Services.AddSingleton(typeof(IBookRepository), typeof(BookRepository));


#endregion

startup.ConfigureServices(builder.Services); // calling ConfigureServices method
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.Run();


