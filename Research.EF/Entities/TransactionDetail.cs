﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Research.EF.Entities
{
    public class TransactionDetail
    {
        [Key]
        public int TransactionDetailID { get; set; }

        [ForeignKey("TransactionID")]
        public int TransactionID { get; set; }

        [ForeignKey("BookTransactionDetailLinkID")]
        public int BookTransactionDetailLinkID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Guid { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        [DefaultValue(0)]
        public int Amount { get; set; }

        [DefaultValue(0.0)]
        public double CostEach { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public virtual Transaction Transaction { get; set; }
        public ICollection<BookTransactionDetailLink> BookTransactionDetailLinks  { get; set; }
    }
}
