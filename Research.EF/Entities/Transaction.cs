﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Research.EF.Entities
{
    public class Transaction
    {
        [Key]
        public int TransactionID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Guid { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        [DefaultValue(0.0)]
        public double TotalPrice { get; set; }

        [DefaultValue(0.0)]
        public double Discount { get; set; }    

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }  

        public ICollection<TransactionDetail> TransactionDetails { get; set; }
        public Voucher Voucher { get; set; }
    }
}
