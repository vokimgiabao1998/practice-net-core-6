﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Research.EF.Entities
{
    public class BookTransactionDetailLink
    {
        [Key]
        public int BookTransactionDetailLinkID { get; set; }
        public int BookID { get; set; }
        public int TransactionDetailID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Guid { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        public virtual Book Book { get; set; }
        public virtual TransactionDetail TransactionDetail { get; set; }

    }
}
