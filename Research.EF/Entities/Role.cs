﻿using Research.EF.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Research.EF.Entities
{
    public class Role
    {
        [Key]
        public int RoleID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Guid { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public EnumType Type { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
    }
}
