﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Research.EF.Entities;

namespace Research.EF.DbConfiguration
{
    public class VoucherConfiguration : IEntityTypeConfiguration<Voucher>
    {
        public void Configure(EntityTypeBuilder<Voucher> builder)
        {
            builder.ToTable("Vouchers");

            builder.HasKey(p => p.VoucherID);

            builder.Property(p => p.Guid)
               .HasDefaultValue(Guid.NewGuid());

            builder.Property(p => p.DateCreated)
                .HasDefaultValue(DateTime.Now);

            builder.Property(p => p.DateModified)
                .HasDefaultValue(DateTime.Now);

            builder.Property(p => p.Code)
                .IsRequired()
                .HasDefaultValue("");

            builder.Property(p => p.Value)
               .IsRequired()
               .HasDefaultValue(0.0);

            builder.Property(p => p.IsActive)
                .IsRequired()
                .HasDefaultValue(false);

            builder.Property(p => p.IsDeleted)
                .IsRequired()
                .HasDefaultValue(false);
        }
    }
}
