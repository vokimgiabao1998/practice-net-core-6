﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Research.EF.Entities;

namespace Research.EF.DbConfiguration
{
    public class TransactionConfiguration : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.ToTable("Transactions");

            builder.HasKey(p => p.TransactionID);

            builder.Property(p=> p.Guid)
                .HasDefaultValue(Guid.NewGuid());

            builder.Property(p => p.DateCreated)
                .HasDefaultValue(DateTime.Now);

            builder.Property(p => p.DateModified)
                .HasDefaultValue(DateTime.Now);

            builder.Property(p => p.TotalPrice)
                .IsRequired()
                .HasDefaultValue(0.0);

            builder.Property(p => p.IsDeleted)
                .IsRequired()
                .HasDefaultValue(false);
        }
    }
}
