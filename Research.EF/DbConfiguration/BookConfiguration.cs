﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Research.EF.Entities;

namespace Research.EF.DbConfiguration
{
    public class BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.ToTable("Books");

            builder.HasKey(s => s.BookID);

            builder.Property(p => p.Guid)
                .HasDefaultValue(Guid.NewGuid());

            builder.Property(p => p.IsActive)
                .IsRequired()
                .HasDefaultValue(false);

            builder.Property(p => p.IsDeleted)
                .IsRequired()
                .HasDefaultValue(false);

            builder.Property(p => p.DateCreated)
                .HasDefaultValue(DateTime.Now);

            builder.Property(p => p.DateModified)
                .HasDefaultValue(DateTime.Now);

            builder.Property(p => p.BookName)
                .IsRequired()
                .HasMaxLength(300);
        }
    }
}
