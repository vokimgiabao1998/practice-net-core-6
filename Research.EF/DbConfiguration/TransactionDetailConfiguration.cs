﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Research.EF.Entities;

namespace Research.EF.DbConfiguration
{
    public class TransactionDetailConfiguration : IEntityTypeConfiguration<TransactionDetail>
    {
        public void Configure(EntityTypeBuilder<TransactionDetail> builder)
        {
            builder.ToTable("TransactionDetails");

            builder.HasKey(p => p.TransactionDetailID);

            builder.Property(p => p.Guid)
                .HasDefaultValue(Guid.NewGuid());

            builder.Property(p => p.DateCreated)
                .HasDefaultValue(DateTime.Now);

            builder.Property(p => p.DateModified)
                .HasDefaultValue(DateTime.Now);

            builder.Property(p => p.Amount)
                .IsRequired()
                .HasDefaultValue(0);

            builder.Property(p => p.CostEach)
                .IsRequired()
                .HasDefaultValue(0.0);

            builder.Property(p => p.IsDeleted)
              .IsRequired()
              .HasDefaultValue(false);

            builder.HasOne(s => s.Transaction)
                .WithMany(t => t.TransactionDetails)
                .HasForeignKey(f => f.TransactionID);

            builder.HasMany(s => s.BookTransactionDetailLinks)
                .WithOne(p => p.TransactionDetail)
                .HasForeignKey(f => f.TransactionDetailID);                
        }
    }
}
