﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Research.EF.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Research.EF.DbConfiguration
{
    public class BookTransactionDetailLinkConfiguration : IEntityTypeConfiguration<BookTransactionDetailLink>
    {
        public void Configure(EntityTypeBuilder<BookTransactionDetailLink> builder)
        {
            builder.ToTable("BookTransactionDetailLinks");

            builder.HasKey(p => p.BookTransactionDetailLinkID);

            builder.Property(p => p.Guid)
               .HasDefaultValue(Guid.NewGuid());

            builder.Property(p => p.DateCreated)
                .HasDefaultValue(DateTime.Now);

            builder.Property(p => p.DateModified)
                .HasDefaultValue(DateTime.Now);

            builder.Property(p => p.IsDeleted)
                .IsRequired()
                .HasDefaultValue(false);

            builder.HasOne(p => p.Book)
                .WithMany(p => p.BookTransactionDetailLinks)
                .HasForeignKey(f => f.BookID);
        }
    }
}
