﻿using Microsoft.EntityFrameworkCore;
using Research.EF.DbConfiguration;
using Research.EF.Entities;

namespace Research.EF
{
    public class EFDbContext:DbContext
    {
        public EFDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BookConfiguration());
            modelBuilder.ApplyConfiguration(new TransactionConfiguration());
            modelBuilder.ApplyConfiguration(new TransactionDetailConfiguration());
            modelBuilder.ApplyConfiguration(new BookTransactionDetailLinkConfiguration());
            modelBuilder.ApplyConfiguration(new VoucherConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionDetail> TransactionDetails { get; set; }
        public DbSet<BookTransactionDetailLink> BookTransactionDetailLinks { get; set; }
        //public DbSet<Role> Roles { get; set; }
        public DbSet<Voucher> Vouchers { get; set; }


    }
}