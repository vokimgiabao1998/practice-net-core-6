﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Research.Repository.Base
{
    public interface IGenericRepository<T> where T : class
    {
        void Add(T entity);
        void AddRange(IQueryable<T> entities);
        void Update(T entity);
        void Delete(T entity);
        void DeleteRange(IQueryable<T> entities);
        void AddRangeAsync(IQueryable<T> entities);
        void AddAsync(T entity);
    }
}
