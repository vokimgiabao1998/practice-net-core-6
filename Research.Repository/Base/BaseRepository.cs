﻿using Research.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Research.Repository.Base
{
    public class BaseRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly EFDbContext _context;
        public BaseRepository(EFDbContext context)
        {
            _context = context;
        }

        public void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        public void AddAsync(T entity)
        {
            _context.Set<T>().AddAsync(entity);
        }

        public void AddRange(IQueryable<T> entities)
        {
            _context.Set<T>().AddRange(entities);
        }

        public void AddRangeAsync(IQueryable<T> entities)
        {
            _context.Set<T>().AddRangeAsync(entities);
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public void DeleteRange(IQueryable<T> entities)
        {
            _context.Set<T>().RemoveRange(entities);
        }

        public void Update(T entity)
        {
            _context.Set<T>().Update(entity);
        }
    }
}
