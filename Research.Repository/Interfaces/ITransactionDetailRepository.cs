﻿using Research.EF.Entities;
using Research.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Research.Repository.Interfaces
{
    public interface ITransactionDetailRepository : IGenericRepository<TransactionDetail>
    {

    }
}
