﻿using Research.EF;
using Research.EF.Entities;
using Research.Repository.Base;
using Research.Repository.Interfaces;

namespace Research.Repository.Repositories
{
    public class BookRepository:BaseRepository<Book>,IBookRepository
    {
        public BookRepository(EFDbContext context): base(context) 
        {

        }

        public void GetNewBook()
        {
            
        }
    }
}
