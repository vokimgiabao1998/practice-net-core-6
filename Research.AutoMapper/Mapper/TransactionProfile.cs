﻿using AutoMapper;
using Research.AutoMapper.DTOs;
using Research.EF.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Research.AutoMapper.Mapper
{
    public class TransactionProfile : Profile
    {
        public TransactionProfile() 
        {
            CreateMap<Transaction, TransactionDTO>().IgnoreAllNonExisting();
        }
    }
}
