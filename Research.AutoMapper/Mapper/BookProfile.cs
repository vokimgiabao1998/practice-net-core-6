﻿using AutoMapper;
using Research.AutoMapper.DTOs;
using Research.EF.Entities;
using System.Reflection;

namespace Research.AutoMapper.Mapper
{
    public class BookProfile : Profile
    {
        public BookProfile() 
        {
            CreateMap<Book, BookDTO>().IgnoreAllNonExisting();
        }
    }
}