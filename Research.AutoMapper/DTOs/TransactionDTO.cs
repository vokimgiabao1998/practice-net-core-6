﻿using AutoMapper;
using Research.EF.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Research.AutoMapper.DTOs
{
    public class TransactionDTO
    {
        public int TransactionID { get; set; }
        public Guid Guid { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public double TotalPrice { get; set; }
        public double Discount { get; set; }
        public bool IsDeleted { get; set; }
    }
}
